package domains

type Profile struct {
	//gorm.Model
	ID          int    `json:"ID"`
	FullNama    string `json:"FullNama"`
	NickName    string `json:"NickName"`
	PhoneNumber string `json:"PhoneNumber"`
	Alamat      string `json:"Alamat"`
	UrlPhoto    string `json:"UrlPhoto"`
}

// TableName overrides the table name used by User to `profiles`
func (Profile) TableName() string {
	return "Profile"
}
