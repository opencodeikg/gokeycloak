package services

import (
	"GoKeycloak/errors"
	"encoding/json"
	"fmt"
	"github.com/Nerzal/gocloak/v8"
	_ "github.com/gorilla/mux"
	"net/http"
	"os"
	"strings"
)

type LoginResponse struct {
	AccessToken string `json:"access_token"`
	Title       string `json:"Title"`
	Description string `json:"Description"`
}

var (
	clientId     = os.Getenv("CLIENT_ID")
	clientSecret = os.Getenv("CLIENT_SECRET")
	realm        = os.Getenv("REALM")
	hostname     = os.Getenv("HOST")
)

var client gocloak.GoCloak

func InitializeOauthServer() {
	client = gocloak.NewClient(hostname)
}

func Protect(next http.Handler) http.Handler {

	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Println("Hello : ")
		authHeader := r.Header.Get("Authorization")

		if len(authHeader) < 1 {
			w.WriteHeader(401)
			json.NewEncoder(w).Encode(errors.UnauthorizedError())
			return
		}

		accessToken := strings.Split(authHeader, " ")[1]
		fmt.Println(" accessToken : ", accessToken)
		fmt.Println(" clientId : ", clientId)
		fmt.Println(" clientSecret : ", clientSecret)
		fmt.Println(" realm : ", realm)
		rptResult, err := client.RetrospectToken(r.Context(), accessToken, clientId, clientSecret, realm)
		fmt.Println(" Response : ", rptResult)

		if err != nil {
			w.WriteHeader(400)
			json.NewEncoder(w).Encode(errors.BadRequestError(err.Error()))
			return
		}

		isTokenValid := *rptResult.Active

		if !isTokenValid {
			w.WriteHeader(401)
			json.NewEncoder(w).Encode(errors.UnauthorizedError())
			return
		}

		// Our middleware logic goes here...
		next.ServeHTTP(w, r)
	})
}
