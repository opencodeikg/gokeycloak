package config

import (
	"GoKeycloak/domains"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

var Database *gorm.DB

func DbConnect() {
	dsn := "iketutg:password@tcp(192.168.57.19:3309)/customer?charset=utf8mb4&parseTime=True&loc=Local"
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	Database = db
	if err != nil {
		panic("failed to connect database")
	}

	//runMigrations()

}

func runMigrations() {
	Database.AutoMigrate(&domains.Profile{})
}
