package controllers

import (
	"GoKeycloak/services"
	"github.com/gorilla/mux"
	"net/http"
)

type EventController struct {
}

func (t EventController) RegisterRoutes(router *mux.Router) {
	router.Handle("/customers", services.Protect(http.HandlerFunc(services.CreateEvent))).Methods("POST")
	router.Handle("/customers/{id}", services.Protect(http.HandlerFunc(services.GetOneEvent))).Methods("GET")
	router.Handle("/customers", services.Protect(http.HandlerFunc(services.AllEvents))).Methods("GET")
}
