package repositories

import (
	"GoKeycloak/config"
	"GoKeycloak/domains"
	"GoKeycloak/errors"
)

func Save(profile *domains.Profile) (*domains.Profile, *errors.HttpError) {

	e := config.Database.Create(&profile)

	if e.Error != nil {
		return nil, errors.DataAccessLayerError(e.Error.Error())
	}

	return profile, nil
}

func FindOneById(id int) *domains.Profile {
	var customer domains.Profile

	config.Database.First(&customer, id)

	return &customer
}

func FindAll() []domains.Profile {
	var profiles []domains.Profile
	config.Database.Find(&profiles)

	return profiles
}
